<?php

namespace WP_Lib\Models;

use WP_Lib\Support\Jsonable;

class BaseModel extends Jsonable {
    protected $builder;
    protected $exists = false;

    public function __construct($exists = false) {
        $this->exists = $exists;

        if (! $exists) {
            $this->initBuilder();
        }
    }

    public function boot(array $attributes = []) {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }

        $this->fireAppends();
    }

    public function newInstance(array $attributes = [], array $appends = [], array $mutators = []) {
        $model = new static(true);

        $model->setAppends($appends);

        $model->setMutators($mutators);

        $model->boot($attributes);

        return $model;
    }

    public static function hydrate(array $items, array $appends, array $mutators) {
        $instance = new static;

        $items = array_map(function ($item) use ($instance, $appends, $mutators) {
            return $instance->newInstance((array) $item, $appends, $mutators);
        }, $items);

        return collect($items);
    }

    public function toArray() {
        return $this->attributes;
    }

    public function __get($key) {
        return $this->getAttribute($key);
    }

    public function __set($key, $value) {
        $this->setAttribute($key, $value);
    }

    public function __isset($key) {
        return isset($this->attributes[$key]) ?? null;
    }

    public function __unset($key) {
        unset($this->attributes[$key]);
    }

    public function __call($method, $parameters) {
        if (method_exists($this, $method)) {
            return call_user_func_array([$this, $method], $parameters);
        }

        if ($this->exists) {
            return;
        }

        return call_user_func_array([$this->builder, $method], $parameters);
    }

    public static function __callStatic($method, $parameters) {
        $instance = new static;

        return call_user_func_array([$instance, $method], $parameters);
    }
}
