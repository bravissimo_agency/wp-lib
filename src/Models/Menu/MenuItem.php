<?php

namespace WP_Lib\Models\Menu;

use WP_Lib\Support\Jsonable;

class MenuItem extends Jsonable {
    public $id;
    public $url;
    public $target;
    public $title;
    public $classes;
    public $childs;
    public $description;

    public function __construct($id, $url, $target, $title, $classes, $description) {
        $this->id = $id;
        $this->url = $url;
        $this->target = $target;
        $this->title = $title;
        $this->classes = $classes;
        $this->description = $description;
    }

    public function addChilds($childs) {
        $this->childs = collect($childs);
    }

    public function hasChilds() {
        return $this->childs && $this->childs->count() > 0;
    }

    public function toArray() {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'target' => $this->target,
            'title' => $this->title,
            'classes' => $this->classes,
            'description' => $this->description,
            'childs' => $this->childs,
        ];
    }
}
