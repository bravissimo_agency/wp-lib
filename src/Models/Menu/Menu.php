<?php

namespace WP_Lib\Models\Menu;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Menu {
    protected $id;
    protected $addActiveClass;
    protected $relativeLinks;
    protected $includeChilds;
    protected $menuItems;
    protected $homeUrl;

    public function __construct() {
        $this->homeUrl = getenv('WP_HOME');
    }

    public function find(string $id) : Menu {
        $this->id = $id;

        return $this;
    }

    public function relativeLinks() : Menu {
        $this->relativeLinks = true;

        return $this;
    }

    public function addActiveClass() : Menu {
        $this->addActiveClass = true;

        return $this;
    }

    public function withChilds() : Menu {
        $this->includeChilds = true;

        return $this;
    }

    public function get() : ?Collection {
        $locations = get_nav_menu_locations();

        if (! isset($locations[$this->id])) {
            $this->menuItems = wp_get_nav_menu_items($this->id);
        } else {
            $menu = get_term($locations[$this->id]);
            $this->menuItems = wp_get_nav_menu_items($menu->term_id);
        }

        if (! $this->menuItems) {
            return null;
        }

        return collect($this->addItems(0));
    }

    protected function addItems(int $parentId) : array {
        $items = [];

        foreach ($this->menuItems as $key => $item) {
            if ((int) $item->menu_item_parent === $parentId) {
                $items[] = $this->addMenuItem($item);
            }
        }

        return $items;
    }

    protected function addMenuItem(object $item) : MenuItem {
        $menuItem = new MenuItem(
            $item->ID,
            $this->getItemLink($item),
            $item->target,
            html_entity_decode($item->title, ENT_QUOTES),
            $this->getItemClasses($item),
            $item->description
        );

        if ($this->includeChilds) {
            $childs = $this->addItems($item->ID);

            if ($childs) {
                $menuItem->addChilds($childs);
            }
        }

        return $menuItem;
    }

    protected function getItemLink(object $item) : string {
        return $this->relativeLinks ? makeLinkRelative($item->url) : $item->url;
    }

    protected function getItemClasses(object $item) :string {
        $classes = array_filter($item->classes);

        if ($this->addActiveClass && $this->isActive($item)) {
            $classes[] = 'isActive';

            if ($this->isExcactActive($item)) {
                $classes[] = 'isExcactActive';
            }
        }

        return implode(' ', $classes);
    }

    protected function isActive(object $item) : bool {
        $permalink = $this->getCurrentPermalink();

        if (isFrontpage($item->object_id) && $item->type === 'post_type') {
            return Str::finish($permalink, '/') === Str::finish($this->homeUrl, '/');
        }

        return strpos($permalink, $item->url) !== false;
    }

    protected function isExcactActive(object $item) : bool {
        $permalink = $this->getCurrentPermalink();

        if (isFrontpage($item->object_id) && $item->type === 'post_type') {
            return Str::finish($permalink, '/') === Str::finish($this->homeUrl, '/');
        }

        return $permalink === $item->url;
    }

    protected function getCurrentPermalink() : string {
        if (is_tax()) {
            return get_term_link(get_queried_object()->term_id);
        }

        return get_the_permalink();
    }
}
