<?php

namespace WP_Lib\Models\Acf;

use WP_Lib\Support\Jsonable;

class File extends Jsonable {
    public $url;
    public $title;
    public $type;
    protected $bytes;

    public function __construct($file) {
        $this->url = $file['url'];
        $this->title = $file['title'];
        $this->type = $this->getTypeFromUrl($file['url']);
        $this->bytes = $file['filesize'];
    }

    public function size(int $decimals = 2) : string {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen($this->bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $this->bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
    }

    public function toArray() {
        return [
            'url' => $this->url,
            'title' => $this->title,
            'type' => $this->type,
            'bytes' => $this->bytes,
            'size' => $this->size(),
        ];
    }

    protected function getTypeFromUrl(string $url) : string {
        return pathinfo($url, PATHINFO_EXTENSION);
    }
}
