<?php

namespace WP_Lib\Models\Taxonomy;

use WP_Lib\Support\Acf;

class Builder {
    protected $findId;
    protected $name;
    protected $postId;
    protected $orderBy;
    protected $order = 'asc';
    protected $acf;
    protected $hideEmpty = true;
    protected $includeChilds = false;
    protected $permalink;
    protected $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function find(int $id) : Term {
        $this->findId = $id;

        return $this->model;
    }

    public function when($value, $callback) {
        if ($value) {
            return $callback($this->model);
        }

        return $this->model;
    }

    public function name(string $name) : Term {
        $this->name = $name;

        return $this->model;
    }

    public function forPost(int $postId) : Term {
        $this->postId = $postId;

        return $this->model;
    }

    public function order(string $order) : Term {
        $this->order = $order;

        return $this->model;
    }

    public function orderBy(string $orderBy, ?string $order = 'asc') : Term {
        $this->orderBy = $orderBy;
        $this->order = $order;

        return $this->model;
    }

    public function orderBySortOrder(string $order = 'asc') : Term {
        $this->orderBy = 'sortorder';
        $this->order = $order;

        return $this->model;
    }

    public function orderByName(string $order = 'asc') : Term {
        $this->orderBy = 'name';
        $this->order = $order;

        return $this->model;
    }

    public function withAcf($fields = true) : Term {
        $this->acf = $fields;

        return $this->model;
    }

    public function showEmpty() : Term {
        $this->hideEmpty = false;

        return $this->model;
    }

    public function withPermalink() : Term {
        $this->permalink = true;

        return $this->model;
    }

    public function withChilds() : Term {
        $this->includeChilds = true;

        return $this->model;
    }

    public function getItems() {
        if ($this->findId) {
            return [$this->setupTerm(get_term($this->findId))];
        }

        $terms = $this->getTerms();

        if (! $terms) {
            return [];
        }

        $terms = array_map(function ($term) {
            return $this->setupTerm($term);
        }, $terms);

        if ($this->orderBy) {
            $terms = $this->sortTerms($terms);
        }

        if (! $this->includeChilds) {
            return $terms;
        }

        $out = [];

        foreach ($terms as $key => $term) {
            if ($term->parent === 0) {
                $term->childs = $this->findChilds($terms, $term->id);
                $out[] = $term;
            }
        }

        return $out;
    }

    public function get() {
        return $this->model::hydrate($this->getItems(), $this->model->getAppends(), $this->model->getMutators());
    }

    public function first() {
        return $this->get()->first();
    }

    protected function findChilds(array $terms, int $termId) : array {
        $childs = [];

        foreach ($terms as $key => $term) {
            if ($term->parent === $termId) {
                $term->childs = $this->findChilds($terms, $term->id);
                $childs[] = $term;
            }
        }

        return $childs;
    }

    protected function getTerms() : ?array {
        if ($this->postId) {
            $terms = get_the_terms($this->postId, $this->name);

            return is_array($terms) ? $terms : null;
        }

        return get_terms([
            'taxonomy' => $this->name,
            'hide_empty' => $this->hideEmpty,
        ]);
    }

    protected function setupTerm(object $term) : object {
        $item = [
            'id' => $term->term_id,
            'name' => html_entity_decode($term->name, ENT_QUOTES),
            'slug' => $term->slug,
            'taxonomy' => $term->taxonomy,
            'count' => $term->count,
            'parent' => $term->parent,
        ];

        if ($this->acf) {
            $item = array_merge($item, (new Acf)->getFields($this->acf, $term));
        }

        if ($this->permalink) {
            $item['permalink'] = $this->getPermalink($term->term_id);
        }

        return (object) $item;
    }

    protected function sortTerms(array $terms) : array {
        if ($this->orderBy === 'sortorder') {
            return $this->sortTermsBySortOrder($terms);
        }

        if ($this->orderBy === 'name') {
            return $this->sortTermsByName($terms);
        }

        return $terms;
    }

    protected function sortTermsBySortOrder(array $terms) : array {
        $terms = array_map(function ($term) {
            $term->sortorder = getTermSortOrder($term->taxonomy, $term->id);

            return $term;
        }, $terms);

        usort($terms, function ($a, $b) {
            if ($this->order === 'desc') {
                return $b->sortorder - $a->sortorder;
            }

            return $a->sortorder - $b->sortorder;
        });

        return $terms;
    }

    protected function sortTermsByName(array $terms) : array {
        usort($terms, function ($a, $b) {
            if ($this->order === 'desc') {
                return strcasecmp($b->name, $a->name);
            }

            return strcasecmp($a->name, $b->name);
        });

        return $terms;
    }

    protected function getPermalink(int $id) : string {
        return get_term_link($id);
    }
}
