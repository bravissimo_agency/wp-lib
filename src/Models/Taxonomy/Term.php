<?php

namespace WP_Lib\Models\Taxonomy;

use WP_Lib\Models\BaseModel;
use WP_Lib\Support\Traits\HasAttributes;

class Term extends BaseModel {
    use HasAttributes;

    public function initBuilder() {
        $this->builder = new Builder($this);
        $this->builder->name($this->taxonomy);
    }
}
