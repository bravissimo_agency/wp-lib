<?php

namespace WP_Lib\Models\Post;

use WP_Lib\Models\BaseModel;
use WP_Lib\Support\Traits\HasAttributes;

class Model extends BaseModel
{
    use HasAttributes;

    public function initBuilder()
    {
        $this->builder = new Builder($this);
        $this->builder->postType($this->postType);
    }

    public function acf(string $field)
    {
        if (!$this->{$field}) {
            $this->{$field} = get_field($field, $this->id);
        }

        return $this->{$field};
    }
}
