<?php

namespace WP_Lib\Notifications;

class Notifications
{
    protected $settings;

    public function __construct() {
        $this->settings = config('notifications');

        add_action('admin_menu', function () {
            add_menu_page(
                $this->settings['title'] ?? 'Notifications',
                $this->settings['title'] ?? 'Notifications',
                $this->settings['capability'] ?? 'edit_pages',
                $this->settings['slug'] ?? 'notifications',
                [$this, 'show'],
                'dashicons-testimonial',
                $this->settings['position'] ?? 100
            );
        });

        add_action('init', function () {
            register_post_type('notification', [
                'labels' => [
                    'name' => 'Notifications',
                    'singular_name' => 'Notification',
                ],
                'public' => false,
                'has_archive' => false,
                'show_ui' => false,
                'show_in_menu' => false,
            ]);
        });

        add_action('wp_ajax_removeNotification', function () {
            $id = request('id');

            wp_trash_post($id);

            wp_send_json('success');
            wp_die();
        });

        if (request('download')) {
            $file = request('download');

            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                exit;
            }

            return;
        }

        if (request('export')) {
            $this->exportToXls();

            return;
        }
    }

    public function exportToXls() {
        $id = request('id') ?? 'all';
        $notifications = $this->getNotifications($id);

        $html = '<table>';

        $keys = [];

        foreach ($notifications as $key => $notification) {
            if ($key === 0) {
                $html .= '<tr>';
                foreach ($notification['data'] as $property => $value) {
                    $html .= "<td>{$property}</td>";
                    $keys[] = $property;
                }
                $keys[] = 'date';
                $html .= '<td>date</td>';
                $html .= '</tr>';
            }

            $html .= '<tr>';
            foreach ($keys as $property) {
                if ($property === 'date') {
                    $html .= "<td>{$notification['date']}</td>";
                } else {
                    $value = $notification['data'][$property] ?? '-';
                    if (is_array($value)) {
                        $value = json_encode($value);
                    }
                    $html .= "<td>{$value}</td>";
                }
            }
            $html .= '</tr>';
        }

        $html .= '</table>';

        xlsFromHtml($html);
    }

    public function getNotifications(?string $id = null) {
        $notifications = get_posts([
            'post_type' => 'notification',
            'post_status' => 'private',
            'posts_per_page' => -1,
            'title' => $id && $id !== 'all' ? $id : null,
        ]);

        return array_map(function ($notification) {
            $data = json_decode($notification->post_content, true);
            $data['date'] = get_the_date('j F Y H:i', $notification->ID);
            $data['id'] = $notification->ID;

            return $data;
        }, $notifications);
    }

    public function show() {
        $page = request('page');
        $id = request('id') ?? 'all';
        $exportUrl = admin_url("admin.php?page={$page}&export=true&id={$id}");
        $notifications = $this->getNotifications($id);
        $filters = config('notifications.filters'); ?>

        <style>
            .heading {
                margin: 30px 0 40px;
            }

            .notification {
                padding: 0;
                margin-top: 0;
                margin-bottom: 10px;
            }

            .notificationHeader {
                position: relative;
            }

            .notificationHeader__inner {
                display: flex;
                align-items: center;
                justify-content: space-between;
                cursor: pointer;
                padding: 15px 20px;
            }

            .notificationHeader__text {
                width: calc(100% - 24px);
                padding-right: 30px;
            }

            .notificationHeader__icon {
                width: 24px;
                height: 24px;
                flex-shrink: 0;
                transition: 0.25s;
            }

            .notificationHeader__inner.isOpen .notificationHeader__icon {
                transform: rotate(0.5turn);
            }

            .notificationBody {
                padding: 5px 20px;
                border-top: 1px solid #ccd0d4;
            }

            .deleteButton {
                border: 0;
                box-shadow: none;
                background-color: transparent;
                width: 24px;
                height: 24px;
                padding: 0;
                position: absolute;
                top: 50%;
                margin-top: -12px;
                right: -50px;
                cursor: pointer;
                transition: 0.25s;
            }

            .deleteButton:hover {
                opacity: 0.5;
            }

            .deleteButton:focus {
                outline: 0;
            }

            .deleteButton svg {
                width: 100%;
                height: 100%;
            }
        </style>

        <div class="wrap">
            <div style="margin-bottom: 15px">
                <h1 class="wp-heading-inline">
                    <?= $this->settings['title'] ?? 'Notifications' ?>
                </h1>
                <a href="<?= $exportUrl; ?>" target="_blank" class="page-title-action">
                    Exportera
                </a>

                <hr class="wp-header-end">
            </div>

            <?php if (! empty($filters)) : ?>
                <div class="tablenav top" style="margin-bottom: 25px">
                    <div class="actions">
                        <select name="formId" id="filterById">
                            <option 
                                value="all"
                                <?= ! request('id') || request('id') === 'all' ? 'selected="selected"' : '' ?>
                            >
                                Visa alla formulär
                            </option>
                            <?php foreach ($filters as $value => $label) : ?>
                                <option
                                    value="<?= $value ?>"
                                    <?= request('id') === $value ? 'selected="selected"' : '' ?> 
                                >
                                    <?= $label ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php endif ?>

            <?php foreach ($notifications as $notification) : ?>
                <div class="card notification" style="width:800px;max-width:99%;">
                    <div class="notificationHeader">
                        <div class="notificationHeader__inner">
                            <div class="notificationHeader__text">
                                <h2 style="margin-bottom: 6px;margin-top: 0;">
                                    <?= $notification['title'] ?>
                                </h2>
                                <p style="margin: 0">
                                    <?= $notification['date'] ?>
                                </p>
                            </div>

                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="notificationHeader__icon"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </div>

                        <button
                            data-id="<?= $notification['id'] ?>"
                            type="button"
                            class="deleteButton"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </button>
                    </div>

                    <div class="notificationBody" style="display: none">
                        <?php foreach ($notification['rows'] as $row) : ?>
                            <p>
                                <?= $row; ?>
                            </p>
                        <?php endforeach; ?>

                        <?php if (! empty($notification['data']['files'])) : ?>
                            <?php foreach ($notification['data']['files'] as $file) : ?>
                                <p>
                                    <a class="downloadFile" href="#" data-path="<?= $file['path']; ?>">
                                        <?= $file['label']; ?>
                                    </a>
                                </p>
                            <?php endforeach; ?>
                        <?php endif ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <script type="text/javascript">
            jQuery('.notificationHeader__inner').click(function () {
                jQuery(this).toggleClass('isOpen');
                jQuery(this).closest('.notification').find('.notificationBody').slideToggle(250);
            });

            jQuery('.deleteButton').click(function () {
                if (window.confirm('Bekräfta att du vill fortsätta med borttagningen')) {
                    var id = jQuery(this).data('id');
                    jQuery(this).closest('.notification').remove();

                    jQuery.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: ajaxurl,
                        data: {
                            'action': 'removeNotification',
                            'id': id,
                        },
                        success: function(result) {
                            console.log(result);
                        }
                    });
                }
            });

            jQuery('#filterById').change(function () {
                window.location.href += '&id=' + this.value;
            });

            jQuery('.downloadFile').click(function (event) {
                event.preventDefault();
                window.location.href += '&download=' + this.dataset.path;
            });
        </script>
        <?php
    }
}
