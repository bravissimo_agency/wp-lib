<?php

namespace WP_Lib\Notifications;

use Illuminate\Support\Str;

class Notification {
    public function notify() {
        foreach ($this->via() as $type) {
            if ($type === 'database') {
                $this->storeDatabase();
            } elseif ($type === 'mail') {
                $this->sendMail();
            } else {
                $this->checkIfToMethodExists($type);
            }
        }
    }

    protected function sendMail() {
        $this->toMail()->send();
    }

    protected function storeDatabase() {
        wp_insert_post([
            'post_title' => $this->id,
            'post_type' => 'notification',
            'post_status' => 'private',
            'post_content' => json_encode($this->toDatabase(), JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT),
        ]);
    }

    protected function checkIfToMethodExists($name) {
        $method = 'to' . Str::studly($name);

        if (method_exists($this, $method)) {
            $this->{$method}();
        }
    }
}
