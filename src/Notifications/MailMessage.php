<?php

namespace WP_Lib\Notifications;

use WP_Lib\Mail\Mailer;

class MailMessage extends Mailer
{
    public $rows;
    public $attachments = [];

    public function greeting($greeting) {
        $this->rows[] = [
            'type' => 'greeting',
            'value' => $greeting,
        ];

        return $this;
    }

    public function line($line) {
        $this->rows[] = [
            'type' => 'line',
            'value' => $line,
        ];

        return $this;
    }

    public function lines($lines) {
        $this->rows[] = [
            'type' => 'lines',
            'lines' => $lines,
        ];

        return $this;
    }

    public function panel($panel) {
        $this->rows[] = [
            'type' => 'panel',
            'value' => $panel,
        ];

        return $this;
    }

    public function button($button) {
        $this->rows[] = [
            'type' => 'button',
            'value' => $button,
        ];

        return $this;
    }

    public function attachment($file) {
        if (! function_exists('wp_handle_upload')) {
            require_once ABSPATH . 'wp-admin/includes/file.php';
        }

        $changeUploadFolder = function ($dirs) {
            $dirs['basedir'] = str_replace('/public_html/wp-content/uploads', '/storage', $dirs['basedir']);
            $dirs['baseurl'] = str_replace('/wp-content/uploads', '/storage', $dirs['baseurl']);

            $dirs['subdir'] = '/form-uploads';
            $dirs['path'] = $dirs['basedir'] . '/form-uploads';
            $dirs['url'] = $dirs['baseurl'] . '/form-uploads';

            return $dirs;
        };

        add_filter('upload_dir', $changeUploadFolder);

        $uploadedFile = wp_handle_upload($file, ['test_form' => false]);

        remove_filter('upload_dir', $changeUploadFolder);

        $this->attachments[] = $uploadedFile['file'];

        return $uploadedFile['file'];
    }

    public function getBody() : string {
        return render('emails.notification', ['rows' => $this->rows]);
    }
}
