��    $      <  5   \      0     1     A     N  @   \     �  $   �     �     �     �     �          %     9     V     b     }  "   �     �     �     �  #   �       $   '     L     d     }     �     �     �     �     �       0   &  B   W     �  �  �     v  	   �  
   �  F   �     �  4   �     	  #   "	     F	     c	     	     �	      �	     �	     �	     
  *   "
     M
  !   _
     �
  "   �
     �
  *   �
      �
        '   =     e     �     �     �     �  #   �  9     V   @     �     !      "          #      
                                          	                         $                                                                                      %s Archive Page Archive Page Archive Pages One or more fields are not filled in correctly, please try again Save Something went wrong try again later Sort Sort order updated contains invalid characters contains invalid value is not a valid URL is not a valid date is not a valid email address is required must be %d characters long must be a boolean must be a valid credit card number must be accepted must be an active domain must be an integer must be at least %d characters long must be at least %s must be between %d and %d characters must be date after '%s' must be date before '%s' must be date with format '%s' must be different than '%s' must be no more than %s must be numeric must be the same as '%s' must contain %s must contain only letters a-z must contain only letters a-z and/or numbers 0-9 must contain only letters a-z, numbers 0-9, dashes and underscores must not exceed %d characters Project-Id-Version: WP Lib
Report-Msgid-Bugs-To: BUG-EMAIL-ADDR <EMAIL@ADDRESS>
PO-Revision-Date: 2020-09-04 14:00+0200
Last-Translator: 
Language-Team: Bravissimo
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 %s Arkivsida Arkivsida Arkivsidor Ett eller flera fält är inte ifyllt korrekt, vänligen försök igen Spara Något gick fel, vänligen försök igen om en stund Sortera Sorteringsordningen har uppdaterats innehåller ogiltliga tecken innehåller ogiltigt värde är inte en giltlig URL är inte ett giltligt datum är inte en giltlig e-postadress är obligatorisk måste vara %d tecken långt måste vara ett booleskt värde måste vara ett giltligt kredittkortnummer måste accepteras måste vara ett aktivt domännamn måste vara ett heltal måste vara minst %d tecken långt måste vara minst %s måste vara mellan %d och %d tecken långt måste vara ett datum efter '%s' måste vara ett datum före '%s' måste vara ett datum med formatet '%s' får inte vara samma som '%s' får inte vara mer än %s måste vara numerisk måste vara samma som '%s' måste innehålla %s får bara innehålla bokstäver a-z får bara innehålla bokstäver a-z och/eller siffror 0-9 får bara innehålla bokstäver a-z och/eller siffror 0-9, bindestreck och understreck får inte överstiga %d tecken 