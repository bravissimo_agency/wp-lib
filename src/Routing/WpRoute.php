<?php

namespace WP_Lib\Routing;

use Illuminate\Support\Str;

class WpRoute {
    public static function isMatching($name) {
        if (Str::startsWith($name, 'template-') && is_page_template(Str::after($name, 'template-'))) {
            return true;
        }

        if (is_home() && ! is_front_page() && $name === 'archive-post') {
            return true;
        }

        if (is_singular() && Str::startsWith($name, 'single')) {
            return is_singular(Str::after($name, 'single-'));
        }

        if (is_post_type_archive() && Str::startsWith($name, 'archive')) {
            return is_post_type_archive(Str::after($name, 'archive-'));
        }

        if (is_tax() && Str::startsWith($name, 'taxonomy')) {
            return is_tax(Str::after($name, 'taxonomy-'));
        }

        $routeChecks = [
            '404' => fn () => is_404(),
            'search' => fn () => is_search(),
            'front-page' => fn () => is_front_page(),
            'page' => fn () => is_page() && ! is_page_template(),
        ];

        return isset($routeChecks[$name]) && $routeChecks[$name]();
    }

    public static function addRewrite($route) {
        $handler = $route->handler;
        $path = $route->path;
        $name = Str::slug($route->path, '_');

        if (str_contains($path, '{id}')) {
            $path = Str::replace('{id}', '([^/]+)', $path);

            add_action('init', function () use($name, $path) {
                add_rewrite_rule($path, 'index.php?'.$name.'=$matches[1]', 'top');
            }); 

            add_filter('query_vars', function ($vars) use($name) {
                $vars[] = $name;

                return $vars;
            });

            add_action('template_redirect', function () use($name, $handler) {
                if (get_query_var($name)) {
                    [$class, $method] = $handler;
                    (new $class)->{$method}(get_query_var($name));
                    exit();
                }
            });
        } else {
            $path = "^{$path}/?$";

            add_action('init', function () use($name, $path) {
                add_rewrite_rule($path, 'index.php?'.$name.'=true', 'top');
            }); 

            add_filter('query_vars', function ($vars) use($name) {
                $vars[] = $name;

                return $vars;
            });

            add_action('template_redirect', function () use($name, $handler) {
                if (get_query_var($name)) {
                    [$class, $method] = $handler;
                    (new $class)->{$method}();
                    exit();
                }
            });
        }
    }

    public static function init($handler) {
        [$class, $method] = $handler;

        return (new $class)->{$method}();
    }
}
