<?php

namespace WP_Lib\Routing;

use Illuminate\Support\Str;

class Route
{
    public static function createRoute(string $method, string $route, array $handler) : void {
        $routeParts = explode('/', $route);
        $lastRoutePart = end($routeParts);
        $paramName = null;

        if (self::hasParam($lastRoutePart)) {
            $paramName = self::getParamName($lastRoutePart);
            $route = self::getRouteWithParam($route, $lastRoutePart, $paramName);
        }

        register_rest_route('api', $route, [
            'methods' => $method,
            'permission_callback' => '__return_true',
            'callback' => function ($request) use ($handler, $paramName) {
                return self::handleRoute($handler, self::getRequest($request, $paramName));
            },
        ]);
    }

    private static function handleRoute(array $handler, $request) {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler);
        $whoops->register();

        $class = $handler[0];
        $method = $handler[1];

        return (new $class)->{$method}($request);
    }

    private static function getRequest(?object $request = null, ?string $paramName = null) {
        return $paramName ? $request[$paramName] : $request;
    }

    private static function hasParam(string $lastRoutePart) : bool {
        return Str::startsWith($lastRoutePart, '{') && Str::endsWith($lastRoutePart, '}');
    }

    private static function getParamName(string $lastRoutePart) : string {
        return str_replace(['{', '}'], '', $lastRoutePart);
    }

    private static function getRouteWithParam(string $route, string $lastRoutePart, string $paramName) : string {
        return str_replace($lastRoutePart, '(?P<' . $paramName . '>\S+)', $route);
    }
}
