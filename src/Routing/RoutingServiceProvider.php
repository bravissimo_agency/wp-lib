<?php

namespace WP_Lib\Routing;

use Illuminate\Support\ServiceProvider;

class RoutingServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->singleton('router', function () {
            return new Router();
        });
    }

    public function boot() {
        add_action('rest_api_init', function () {
            $this->app->router->initApi();
        });

        $this->app->router->initRewrites();

        add_filter('template_include', function ($template) {
            return $this->app->router->initWp($template);
        });
    }
}
