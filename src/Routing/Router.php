<?php

namespace WP_Lib\Routing;

class Router {
    protected $routes = [];

    public function wp(string $name, array $handler) : void {
        $this->addRoute('WP', $name, $handler);
    }

    public function rewrite(string $name, array $handler) : void {
        $this->addRoute('REWRITE', $name, $handler);
    }

    public function get(string $route, array $handler) : void {
        $this->addRoute('GET', $route, $handler);
    }

    public function post(string $route, array $handler) : void {
        $this->addRoute('POST', $route, $handler);
    }

    public function put(string $route, array $handler) : void {
        $this->addRoute('PUT', $route, $handler);
    }

    public function patch(string $route, array $handler) : void {
        $this->addRoute('PATCH', $route, $handler);
    }

    public function delete(string $route, array $handler) : void {
        $this->addRoute('DELETE', $route, $handler);
    }

    public function addRoute($verb, $route, $handler) {
        $this->routes[] = (object) [
            'verb' => $verb,
            'path' => $route,
            'handler' => $handler,
        ];
    }

    public function loadApiFrom($path) {
        include $path;
    }

    public function loadWpFrom($path) {
        include $path;
    }

    public function initWp($template) {
        $route = collect($this->routes)->filter(function ($route) {
            return $route->verb === 'WP';
        })->first(function ($route) {
            return WpRoute::isMatching($route->path);
        });

        if ($route) {
            return WpRoute::init($route->handler);
        }

        return $template;
    }

    public function initRewrites() {
        collect($this->routes)->filter(function ($route) {
            return $route->verb === 'REWRITE';
        })->each(function ($route) {
            WpRoute::addRewrite($route);
        });
    }

    public function initApi() {
        collect($this->routes)->filter(function ($route) {
            return $route->verb !== 'WP';
        })->each(function ($route) {
            Route::createRoute($route->verb, $route->path, $route->handler);
        });
    }
}
