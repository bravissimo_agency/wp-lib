<?php

/*
// Get app container
*/

use Illuminate\Container\Container;
use Illuminate\Support\Str;

function app($abstract = null, array $parameters = []) {
    if (is_null($abstract)) {
        return Container::getInstance();
    }

    return Container::getInstance()->make($abstract, $parameters);
}

/*
// dd inside json api endpoint
*/

function ddApi($data) : void {
    header('Content-Type: text/html; charset=UTF-8');
    dd($data);
}

/*
// Helper function for making links relative
*/

function makeLinkRelative(string $link) : string {
    $homeUrl = config('app.home_url');

    if (empty($link) || empty($homeUrl)) {
        return $link;
    }

    if (strpos(strval($link), strval($homeUrl)) !== false) {
        $link = wp_make_link_relative($link);
    }

    return $link;
}

/*
// Load config file
*/

function config($key = null, $default = null) {
    if (is_null($key)) {
        return app('config');
    }

    if (is_array($key)) {
        return app('config')->set($key);
    }

    return app('config')->get($key, $default);
}

/*
// Terms sortorder helpers
*/

function getTermSortOrder(string $taxonomy, int $termId) : int {
    $sortOrder = get_option($taxonomy . '_' . $termId . '_sortorder');

    if (! $sortOrder) {
        return 9999999;
    }

    return (int) $sortOrder;
}

function sortBySortOrder(array $a, array $b) : int {
    return $a['sortorder'] - $b['sortorder'];
}

/*
// Try to find page based on request can be path or id
*/

function getPostIdFromRequest($request, string $type = 'page') : ?int {
    $res = get_page_by_path($request, OBJECT, $type);
    $postId = null;

    if (! $res && is_numeric($request)) {
        $postId = (int) $request;
    } elseif ($res) {
        $postId = $res->ID;
    }

    if ($postId && get_post_type($postId) !== $type) {
        wp_send_json_error('Page not found');
    }

    return $postId;
}

/*
// Generate and download XLS file from html
*/

function xlsFromHtml($html, $fileName = 'export') {
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
    $spreadsheet = $reader->loadFromString($html);

    foreach (range('A', $spreadsheet->getActiveSheet()->getHighestDataColumn()) as $col) {
        $spreadsheet->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
}

/*
// Helpers for determine if env is prod or dev
*/

function isProd() {
    return ! isDev();
}

function isDev() {
    return WP_ENV === 'development';
}

/*
// Create image model from src
*/

function imageModelFromSrc($src, $fallback = null) {
    $sizes = config('assets.imageSizes');

    $imageSizes = [];

    try {
        [$width, $height] = getimagesize($src);
    } catch (\Throwable $th) {
        if ($fallback) {
            return imageModelFromSrc($fallback);
        }
    }

    if ($fallback && ($width < 2 || $height < 2)) {
        return imageModelFromSrc($fallback);
    }

    foreach ($sizes as $size => $data) {
        $imageSizes[$size] = $src;
        $imageSizes["{$size}-width"] = $width;
        $imageSizes["{$size}-height"] = $height;
    }

    return new WP_Lib\Models\Acf\Image($src, null, null, $imageSizes, false);
}

/*
// Uplad image to media from url
*/

function uploadMediaFromUrl(string $url, string $fileName) : int {
    require_once ABSPATH . 'wp-admin/includes/file.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';

    $uploadDir = wp_upload_dir();
    $uploadFile = $uploadDir['path'] . '/' . $fileName;

    $fileContent = file_get_contents($url);
    $saveFile = fopen($uploadFile, 'w');
    fwrite($saveFile, $fileContent);
    fclose($saveFile);

    $fileType = wp_check_filetype(basename($fileName), null);

    $attachment = [
        'post_mime_type' => $fileType['type'],
        'post_title' => $fileName,
        'post_content' => '',
        'post_status' => 'inherit',
    ];

    $attachmentId = wp_insert_attachment($attachment, $uploadFile);

    $file = get_post($attachmentId);
    $filePath = get_attached_file($file->ID);
    $attachmentData = wp_generate_attachment_metadata($attachmentId, $filePath);

    wp_update_attachment_metadata($attachmentId, $attachmentData);

    return $attachmentId;
}

/*
// Get archive url
*/

function getArchiveUrl(string $postType) : string {
    if ($postType === 'post') {
        return postArchiveUrl();
    }

    return WP_Lib\CustomArchive\ArchivePage::getRoute($postType);
}

/*
// Get archive id
*/

function getArchiveId(string $postType) : ?int {
    if ($postType === 'post') {
        return postArchiveId();
    }

    return WP_Lib\CustomArchive\ArchivePage::getPageId($postType);
}

/*
// Get menu
*/

function menu($name, $childs = false) {
    $menu = new \WP_Lib\Models\Menu\Menu;

    if ($childs) {
        $menu->withChilds();
    }

    return $menu->find($name)
        ->addActiveClass()
        ->get();
}

/*
// Get excerpt
*/

function excerpt(?string $text = '', int $length = 140) : string {
    $text = trim(strip_tags($text));
    $text = preg_replace("/\s+/u", ' ', $text);

    return Str::limit($text, $length);
}

/*
// Get post model from id
*/

function postModel(int $postId, string $model) {
    return (new $model)->find($postId)->first();
}
