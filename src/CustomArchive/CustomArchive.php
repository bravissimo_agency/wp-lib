<?php

namespace WP_Lib\CustomArchive;

use Illuminate\Support\Str;

class CustomArchive {
    public function __construct() {
        add_action('admin_init', function () {
            $supportedPostTypes = $this->getSupportedPostTypes();

            new Admin($supportedPostTypes);
            new ACFLocation($supportedPostTypes);
        });

        add_filter('register_post_type_args', [$this, 'setPostTypeArchive'], 10, 2);
    }

    public function getSupportedPostTypes() {
        $postTypes = get_post_types([
            'has_archive' => true,
            '_builtin' => false,
        ]);

        return collect($postTypes)
            ->filter('post_type_exists')
            ->map('get_post_type_object')
            ->map(function ($postType) {
                return new ArchivePage($postType);
            });
    }

    public static function config() {
        $config = get_option('custom_archive_pages');

        if (is_array($config)) {
            return $config;
        }

        return [];
    }

    public function setPostTypeArchive($args, $name) {
        $archiveRoute = ArchivePage::getRoute($name);

        if (empty($archiveRoute)) {
            return $args;
        }

        $archiveRoute = trim(str_replace(home_url(), '', $archiveRoute), '/');

        if (! $archiveRoute) {
            return $args;
        }

        if (! empty($args['archiveOnly'])) {
            add_action('template_redirect', function () use ($name) {
                if (is_singular($name)) {
                    wp_redirect(get_post_type_archive_link(get_query_var('post_type')), 301);
                    exit;
                }
            });
        }

        $args['has_archive'] = true;

        if (! empty(config('postTypes')[$name]['taxBase'])) {
            $taxBase = config('postTypes')[$name]['taxBase'];

            $archiveRoute .= "/%{$taxBase}%";

            add_filter('post_type_link', function ($postLink, $post) use ($name) {
                $termName = Str::between($postLink, '%', '%');

                if (empty($post) || $post->post_type !== $name) {
                    return str_replace("%{$termName}%", '', $postLink);
                }

                $terms = get_the_terms($post, $termName);

                if (is_wp_error($terms) || empty($terms) || empty($terms[0])) {
                    return str_replace("%{$termName}%", '', $postLink);
                }

                $term = $terms[0];

                return str_replace("%{$termName}%", $term->slug, $postLink);
            }, 1, 3);
        }

        $args['rewrite'] = [
            'slug' => $archiveRoute,
            'with_front' => false,
        ];

        return $args;
    }
}
