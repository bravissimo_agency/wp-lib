<?php

namespace WP_Lib\Support;

use Illuminate\Support\Str;

class Http {
    public static function get(string $url, array $params = []) {
        return self::request('GET', $url, null, $params);
    }

    public static function post(string $url, array $body = [], array $params = []) {
        return self::request('POST', $url, $body, $params);
    }

    public static function put(string $url, array $body = [], array $params = []) {
        return self::request('PUT', $url, $body, $params);
    }

    public static function delete(string $url, array $body = [], array $params = []) {
        return self::request('DELETE', $url, $body, $params);
    }

    protected static function request(string $method, string $url, ?array $body = [], ?array $params = []) {
        if (! empty($params)) {
            $url = self::addParams($url, $params);
        }

        if ($method === 'GET') {
            $res = wp_remote_get($url, ['timeout' => 30]);
        } else {
            $res = wp_remote_post($url, [
                'method' => $method,
                'body' => $body,
                'timeout' => 30,
            ]);
        }

        if (is_wp_error($res)) {
            throw new \Exception('Http request failed . ' . $res->get_error_message());
        }

        return json_decode($res['body']);
    }

    protected static function addParams(string $url, ?array $params) {
        $url .= Str::contains($url, '?') ? '&' : '?';

        return $url . http_build_query ($params);
    }
}
