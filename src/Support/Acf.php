<?php

namespace WP_Lib\Support;

class Acf {
    protected $post;

    public function getFields($fields, object $post) : array {
        $this->post = $post;

        if (is_array($fields)) {
            return $this->getFieldsFromArray($fields);
        }

        return $this->getAllFields();
    }

    protected function getAllFields() : array {
        return get_fields($this->post) ?: [];
    }

    protected function getFieldsFromArray(array $fields) : array {
        return array_reduce($fields, function (array $acc, string $field) {
            $acc[$field] = get_field($field, $this->post);

            return $acc;
        }, []);
    }
}
