<?php

namespace WP_Lib\Support;

class Cache {
    public static function get(string $key, $default = null) {
        $res = get_transient($key);

        if (! $res) {
            return $default;
        }

        return $res;
    }

    public static function has(string $key) {
        return self::get($key) !== false;
    }

    public static function put(string $key, $value, int $duration = HOUR_IN_SECONDS) {
        set_transient($key, $value, $duration);
    }

    public static function forget(string $key) {
        delete_transient($key);
    }

    public static function getOrNew(string $key, callable $callable, int $duration = HOUR_IN_SECONDS) {
        $res = self::get($key);

        if ($res) {
            return $res;
        }

        $res = $callable();
        self::put($key, $res, $duration);

        return $res;
    }
}
