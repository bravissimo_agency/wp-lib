<?php

require_once __DIR__ . './../View/helpers.php';
require_once __DIR__ . './../Request/helpers.php';
require_once __DIR__ . './../helpers.php';

function initApp($rootDir) {
    /**
     * Use Dotenv to set required environment variables and load .env file in root.
     */
    $dotenv = new Dotenv\Dotenv($rootDir);
    if (file_exists($rootDir . '/.env')) {
        $dotenv->load();
        $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL']);
    }

    /*
     * Set up our global environment constant and load its config first
     * Default: production
     */
    define('WP_ENV', getenv('WP_ENV') ?: 'production');

    if (WP_ENV === 'development') {
        define('SAVEQUERIES', true);
        define('WP_DEBUG', true);
        define('SCRIPT_DEBUG', true);
        define('FS_METHOD', 'direct');
    } else {
        ini_set('display_errors', 0);
        define('WP_DEBUG_DISPLAY', false);
        define('SCRIPT_DEBUG', false);
    }

    /*
     * URLs
     */
    define('WP_HOME', getenv('WP_HOME'));
    define('WP_SITEURL', getenv('WP_SITEURL'));

    /*
     * DB settings
     */
    define('DB_NAME', getenv('DB_NAME'));
    define('DB_USER', getenv('DB_USER'));
    define('DB_PASSWORD', getenv('DB_PASSWORD'));
    define('DB_HOST', getenv('DB_HOST') ?: 'localhost');
    define('DB_CHARSET', 'utf8mb4');
    define('DB_COLLATE', '');
    $table_prefix = getenv('DB_PREFIX') ?: 'wp_';

    /*
     * Authentication Unique Keys and Salts
     */
    define('AUTH_KEY', getenv('AUTH_KEY'));
    define('SECURE_AUTH_KEY', getenv('SECURE_AUTH_KEY'));
    define('LOGGED_IN_KEY', getenv('LOGGED_IN_KEY'));
    define('NONCE_KEY', getenv('NONCE_KEY'));
    define('AUTH_SALT', getenv('AUTH_SALT'));
    define('SECURE_AUTH_SALT', getenv('SECURE_AUTH_SALT'));
    define('LOGGED_IN_SALT', getenv('LOGGED_IN_SALT'));
    define('NONCE_SALT', getenv('NONCE_SALT'));

    /*
     * Custom Settings
     */
    define('AUTOMATIC_UPDATER_DISABLED', true);
    define('DISABLE_WP_CRON', getenv('DISABLE_WP_CRON') ?: false);
    define('DISALLOW_FILE_EDIT', true);

    /*
     * Init Sentry for error tracking only in production
     */

    if (WP_ENV === 'production' && ! empty(getenv('SENTRY_DSN'))) {
        \Sentry\init(['dsn' => getenv('SENTRY_DSN')]);
    }

    /*
     * Init error handler for dev
     */

    if (WP_ENV === 'development') {
        $whoops = new \Whoops\Run;
        $whoops->prependHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->register();
    }
}
