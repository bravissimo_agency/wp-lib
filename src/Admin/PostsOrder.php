<?php

namespace WP_Lib\Admin;

class PostsOrder {
    public function __construct($postTypes) {
        $this->init($postTypes);
    }

    public function createOrderPages(array $postTypes) : void {
        foreach ($postTypes as $postType => $settings) {
            $parentSlug = $postType === 'post' ? 'edit.php' : 'edit.php?post_type=' . $postType;

            $label = __('Sort', 'wp_lib') . ' ' . $settings['labels']['name'];

            add_submenu_page(
                $parentSlug,
                $label,
                $label,
                'edit_others_posts',
                "{$postType}-sort",
                function () use ($label, $postType, $settings) {
                    $this->initPostsOrderPage($label, $postType, $settings);
                }
            );
        }
    }

    public function init($postTypes) : void {
        add_action('admin_menu', function () use ($postTypes) {
            $this->createOrderPages($postTypes);
        });

        add_action('wp_ajax_updatePostsOrder', function () {
            $posts = $_POST['posts'];

            foreach ($posts as $post) {
                wp_update_post([
                    'ID'         => $post['id'],
                    'menu_order' => $post['order'],
                ]);
            }

            wp_send_json(1);
            wp_die();
        });
    }

    public function initPostsOrderPage(string $heading, string $postType, array $settings) : void {
        ?>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.4.0/Sortable.min.js"></script>
        <style media="screen">
            .heading {
                margin: 30px 0;
            }
            .list {
                padding-right: 15px;
            }
            .list * {
                box-sizing: border-box;
            }
            .list li {
                background: #fff;
                padding: 10px;
                cursor: move;
                max-width: 100%;
            }
            div.updated {
                margin: 15px 0;
            }
            #success {
                display: none;
            }
            #success.isActive {
                display: block;
            }
        </style>
        <?php

        $posts = get_posts([
            'post_type' => $postType,
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'asc',
            'suppress_filters' => false,
        ]);

        echo "<h1 class='heading'>" . $heading . '</h1>';

        echo '<div class="message updated fade" id="success"><p>' . __('Sort order updated', 'wp_lib') . '</p></div>';

        echo "<ul id='sort-posts' class='list sortable'>";
        foreach ($posts as $key => $post) {
            echo "<li data-id='" . $post->ID . "'>" . $post->post_title . '</li>';
        }
        echo '</ul>';

        echo '<button class="button-primary" id="savePostsOrder">' . __('Save', 'wp_lib') . '</button>'

        ?>
        <script type="text/javascript">
            var button = document.querySelector('#savePostsOrder');
            var list = document.querySelector(".sortable");
    
            button.addEventListener('click', function() {
                updateSortOrder(list);
            });
    
            Sortable.create(list, {
                group: "sorting",
                sort: true,
            });
    
            function updateSortOrder(list) {
                var listItems = list.children;
    
                itemsToUpdate = [];
    
                for (var i = 0; i < listItems.length; i++) {
                    var id = listItems[i].dataset.id;
    
                    var item = {
                        id: id,
                        order: i + 1
                    };
    
                    itemsToUpdate.push(item);
                }
    
                jQuery.ajax({
                   type: 'POST',
                   dataType: 'json',
                   url: ajaxurl,
                   data: {
                       'action': 'updatePostsOrder',
                       'posts': itemsToUpdate,
                   },
                   success: function(result) {
                        if (result) {
                            var notice = document.querySelector('#success');
                            notice.classList.add('isActive');
                            setTimeout(function() {
                                notice.classList.remove('isActive');
                            }, 5000);
                        }
                   }
                });
            }
    
        </script>
        <?php
    }
}
