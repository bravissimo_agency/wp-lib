<?php

namespace WP_Lib\Admin;

class PageOrder {
    public function __construct() {
        $this->init();
    }

    public function createOrderPage() : void {
        $parentSlug = 'edit.php?post_type=page';

        $label = __('Sort', 'wp_lib');

        add_submenu_page(
            $parentSlug,
            $label,
            $label,
            'edit_others_posts',
            'pages-sort',
            function () {
                $this->initPostsOrderPage();
            }
        );
    }

    public function init() : void {
        add_action('admin_menu', function () {
            $this->createOrderPage();
        });

        add_action('wp_ajax_updatePageOrder', function () {
            $posts = $_POST['posts'];

            foreach ($posts as $post) {
                wp_update_post([
                    'ID'         => $post['id'],
                    'menu_order' => $post['order'],
                ]);
            }

            wp_send_json(1);
            wp_die();
        });
    }

    public function renderChilds($post) {
        $posts = get_pages([
            'sort_order' => 'ASC',
            'sort_column' => 'menu_order',
            'parent' => $post->ID,
        ]);

        echo "<ul class='list sortable' style='padding-left: 20px;padding-right:0'>";
        foreach ($posts as $key => $post) {
            echo "<li data-id='" . $post->ID . "'>";

            echo "<span class='title'>{$post->post_title}</span>";

            $this->renderChilds($post);

            echo '</li>';
        }
        echo '</ul>';
    }

    public function initPostsOrderPage() : void {
        ?>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.4.0/Sortable.min.js"></script>
        <style media="screen">
            .heading {
                margin: 30px 0;
            }
            .list {
                padding-right: 15px;
            }
            .list * {
                box-sizing: border-box;
            }
            .list .title {
                background: #fff;
                padding: 10px;
                cursor: move;
                max-width: 100%;
                display: block;
                margin-bottom: 6px;
                padding: 8px 10px;
                font-size: 12px;
            }
            div.updated {
                margin: 15px 0;
            }
            #success {
                display: none;
            }
            #success.isActive {
                display: block;
            }
            .list li {
                margin: 0;
                padding: 0;
            }

            #sort-posts > li {
                margin: 20px 0;
            }

            #sort-posts > li > .title {
                font-size: 14px;
                padding: 10px;
                font-weight: 500;
            }
        </style>
        <?php

        $posts = get_pages([
            'sort_order' => 'ASC',
            'sort_column' => 'menu_order',
            'parent' => 0,
        ]);

        echo "<h1 class='heading'>" . __('Sort', 'wp_lib') . '</h1>';

        echo "<ul id='sort-posts' class='list sortable'>";
        foreach ($posts as $key => $post) {
            echo "<li data-id='" . $post->ID . "'>";

            echo "<span class='title'>{$post->post_title}</span>";

            $this->renderChilds($post);

            echo '</li>';
        }
        echo '</ul>';

        echo '<div class="message updated fade" id="success"><p>' . __('Sort order updated', 'wp_lib') . '</p></div>';

        echo '<button class="button-primary" id="savePostsOrder">' . __('Save', 'wp_lib') . '</button>'

        ?>
        <script type="text/javascript">
            var button = document.querySelector('#savePostsOrder');
            var lists = document.querySelectorAll(".sortable");

    
            button.addEventListener('click', function() {
                updateSortOrder();
            });
            
            lists.forEach((list, i) => {
                Sortable.create(list, {
                    group: `sorting__${i}`,
                    sort: true,
                });
            });

            function updateSortOrder() {
                const oldText = button.innerText;
                button.innerText = 'Loading...';

                var items = document.querySelectorAll('#sort-posts li');

                itemsToUpdate = [];
    
                for (var i = 0; i < items.length; i++) {
                    var id = items[i].dataset.id;
    
                    var item = {
                        id: id,
                        order: i + 1
                    };
    
                    itemsToUpdate.push(item);
                }
    
                jQuery.ajax({
                   type: 'POST',
                   dataType: 'json',
                   url: ajaxurl,
                   data: {
                       'action': 'updatePageOrder',
                       'posts': itemsToUpdate,
                   },
                   success: function(result) {
                        if (result) {
                            var notice = document.querySelector('#success');
                            notice.classList.add('isActive');
                            setTimeout(function() {
                                notice.classList.remove('isActive');
                            }, 5000);
                        }

                        button.innerText = oldText;
                   }
                });
            }
    
        </script>
        <?php
    }
}
