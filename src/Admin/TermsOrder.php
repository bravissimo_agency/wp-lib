<?php

namespace WP_Lib\Admin;

class TermsOrder {
    public function __construct() {
        $taxonomies = config('taxonomies');

        if (empty($taxonomies)) {
            return;
        }

        $this->init($taxonomies);
    }

    public function getOrderPages(array $taxonomies) : array {
        $pages = [];

        foreach ($taxonomies as $taxonomy => $settings) {
            if (isset($settings['sortable']) && $settings['sortable']) {
                $label = $settings['settings']['labels']['name'];

                $postTypes = is_array($settings['post_type']) ? $settings['post_type'] : [$settings['post_type']];

                foreach ($settings['post_type'] as $postType) {
                    $pages[] = [
                        'post_type' => $postType,
                        'label' => __('Sort', 'wp_lib') . ' ' . $label,
                        'taxonomy' => $taxonomy,
                        'page' => "{$postType}-sort-{$taxonomy}",
                    ];
                }
            }
        }

        return $pages;
    }

    public function init(array $taxonomies) : void {
        add_action('admin_menu', function () use ($taxonomies) {
            $pages = $this->getOrderPages($taxonomies);

            if (empty($pages)) {
                return;
            }

            $this->createTermsOrderPages($pages);
        });

        add_action('wp_ajax_updateTermsOrder', function () {
            $items = $_POST['items'];
            $taxonomy = $_POST['taxonomy'];

            foreach ($items as $item) {
                $results = update_option($taxonomy . '_' . $item['id'] . '_sortorder', $item['sortorder']);
            }

            wp_send_json(1);
            wp_die();
        });
    }

    public function createTermsOrderPages(array $items) : void {
        foreach ($items as $item) {
            $parentSlug = $item['post_type'] === 'post' ? 'edit.php' : 'edit.php?post_type=' . $item['post_type'];

            add_submenu_page(
                $parentSlug,
                $item['label'],
                $item['label'],
                'edit_others_posts',
                $item['page'],
                function () use ($item) {
                    $this->initTermsOrderPage($item);
                }
            );
        }
    }

    public function initTermsOrderPage(array $item) : void {
        ?>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@1.4.0/Sortable.min.js"></script>
        <style media="screen">
            .heading {
                margin: 30px 0;
            }
            .list {
                padding-right: 15px;
            }
            .list * {
                box-sizing: border-box;
            }
            .list li {
                background: #fff;
                padding: 10px;
                cursor: move;
                max-width: 100%;
            }
            div.updated {
                margin: 15px 0;
            }
            #success {
                display: none;
            }
            #success.isActive {
                display: block;
            }
        </style>
        <?php

        $termsOrg = get_terms([
            'taxonomy' => $item['taxonomy'],
            'hide_empty' => false,
        ]);

        $terms = [];

        foreach ($termsOrg as $key => $term) {
            $sortOrder = getTermSortOrder($item['taxonomy'], $term->term_id);
            $terms[] = [
                'id' => $term->term_id,
                'name' => $term->name,
                'sortorder' => $sortOrder,
            ];
        }

        usort($terms, 'sortBySortOrder');

        echo "<h1 class='heading'>" . $item['label'] . '</h1>';

        echo '<div class="message updated fade" id="success"><p>' . __('Sort order updated', 'wp_lib') . '</p></div>';

        echo "<ul id='sort-terms' class='list sortable'>";
        foreach ($terms as $key => $term) {
            echo "<li data-id='" . $term['id'] . "'>" . $term['name'] . '</li>';
        }
        echo '</ul>';

        echo '<button class="button-primary" id="saveTermsOrder">' . __('Save', 'wp_lib') . '</button>'

        ?>
        <script type="text/javascript">
            var button = document.querySelector('#saveTermsOrder');
            var list = document.querySelector(".sortable");
    
            button.addEventListener('click', function() {
                updateSortOrder(list);
            });
    
            Sortable.create(list, {
                group: "sorting",
                sort: true,
            });
    
            function updateSortOrder(list) {
                var listItems = list.children;
    
                itemsToUpdate = [];
    
                for (var i = 0; i < listItems.length; i++) {
                    var id = listItems[i].dataset.id;
    
                    var item = {
                        id: id,
                        sortorder: i + 1
                    };
    
                    itemsToUpdate.push(item);
                }
    
                jQuery.ajax({
                   type: 'POST',
                   dataType: 'json',
                   url: ajaxurl,
                   data: {
                       'action': 'updateTermsOrder',
                       'taxonomy': '<?= $item['taxonomy']; ?>',
                       'items': itemsToUpdate,
                   },
                   success: function(result) {
                        if (result) {
                            var notice = document.querySelector('#success');
                            notice.classList.add('isActive');
                            setTimeout(function() {
                                notice.classList.remove('isActive');
                            }, 5000);
                        }
                   }
                });
            }
    
        </script>
        <?php
    }
}
