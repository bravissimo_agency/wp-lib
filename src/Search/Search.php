<?php

namespace WP_Lib\Search;

use Illuminate\Support\Collection;

class Search {
    protected $query;
    protected $postTypes;
    protected $excludeIds;
    protected $take = -1;

    public function query(string $query) : Search {
        $this->query = urldecode($query);

        return $this;
    }

    public function postTypes(array $postTypes) : Search {
        $this->postTypes = $postTypes;

        return $this;
    }

    public function exclude($ids) : Search {
        $this->excludeIds = is_array($ids) ? $ids : [$ids];

        return $this;
    }

    public function take(int $take) : Search {
        $this->take = $take;

        return $this;
    }

    public function get() : Collection {
        $res = new \WP_Query([
            's' => $this->query,
            'post_type' => $this->postTypes,
            'posts_per_page' => $this->take,
            'post_status' => 'publish',
            'post__not_in' => $this->excludeIds,
        ]);

        return collect($res->posts)->map(function ($post) {
            return (object) [
                'id' => $post->ID,
                'postType' => $post->post_type,
            ];
        });
    }
}
