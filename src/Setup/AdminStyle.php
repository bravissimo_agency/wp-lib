<?php

namespace WP_Lib\Setup;

class AdminStyle {
    public function __construct() {
        $this->showCustomDashboard();
        $this->setLoginStyle();
        $this->setFooterText();
    }

    public function showCustomDashboard() : void {
        add_action('wp_dashboard_setup', function () {
            global $wp_meta_boxes;
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
            remove_action('welcome_panel', 'wp_welcome_panel');

            wp_add_dashboard_widget(
                'dashboard_widget',
                get_locale() === 'sv_SE' ? 'Välkommen!' : 'Welcome!',
                function () {
                    if (get_locale() === 'sv_SE') {
                        echo 'Välkommen till administrationsverktyget för ' . get_bloginfo('name');
                    } else {
                        echo 'Welcome to the administration tool for ' . get_bloginfo('name');
                    }
                });
        });
    }

    public function setFooterText() : void {
        add_filter('admin_footer_text', function () {
            echo get_field('footer_text_admin', 'bravissimo');
        });
    }

    public function setLoginStyle() : void {
        add_action('login_enqueue_scripts', function () {
            $backgroundLogin = get_field('login_background', 'bravissimo');
            $logoLogin = get_field('login_logo', 'bravissimo');
            $logoLoginWidth = get_field('login_logo_width', 'bravissimo');
            $logoLoginHeight = get_field('login_logo_height', 'bravissimo');
            $textColor = get_field('login_text_color', 'bravissimo');
            $transparentLogin = get_field('login_transparent_background', 'bravissimo');

            if (! empty($transparentLogin)): ?>

                <style type="text/css">
                    #loginform {
                        background: none;
                        box-shadow: none;
                        border: 0;
                    }
                    #loginform label {
                        color: <?= $textColor ?>
                    }
                </style>

            <?php endif;

            if (! empty($backgroundLogin)): ?>

               <style type="text/css">
                   body.login {
                       background: url(<?= $backgroundLogin ?>) no-repeat center;
                       background-size: cover;
                   }
               </style>

           <?php endif;

            if (! empty($logoLogin)) {
                if ($logoLoginWidth > 320) {
                    $aspectRatio = $logoLoginHeight / $logoLoginWidth;
                    $logoLoginWidth = 260;
                    $logoLoginHeight = $logoLoginWidth * $aspectRatio;
                } ?>

                <style type="text/css">
                    #login h1 a {
                        background-image: url(<?= $logoLogin ?>);
                        width: <?= $logoLoginWidth . 'px;' ?>;
                        height: <?= $logoLoginHeight . 'px;' ?>;
                        -webkit-background-size: <?= $logoLoginWidth . 'px;' ?> <?= $logoLoginHeight . 'px;' ?>;
                        background-size: <?= $logoLoginWidth . 'px;' ?> <?= $logoLoginHeight . 'px;' ?>;
                    }
                </style>

                <?php if (! empty($textColor)): ?>
                    <style type="text/css">
                        #login #backtoblog a, #login #nav a {
                            color: <?= $textColor; ?>;
                        }
                    </style>
                <?php endif; ?>

            <?php
            }
        });
    }
}
