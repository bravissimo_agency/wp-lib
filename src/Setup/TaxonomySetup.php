<?php

namespace WP_Lib\Setup;

class TaxonomySetup {
    public function __construct() {
        add_action('admin_footer', [$this, 'setupTaxonomies']);

        add_filter('wp_terms_checklist_args', function ($args) {
            $args['checked_ontop'] = false;

            return $args;
        }, 10, 2);
    }

    public function setupTaxonomies() : void {
        $taxonomies = config('taxonomies');
        $required = ['category'];
        $radios = [];
        $css = '';

        foreach ($taxonomies as $key => $taxonomy) {
            if (isset($taxonomy['required']) && $taxonomy['required']) {
                $required[] = [
                    'id' => $key,
                    'title' => $taxonomy['settings']['labels']['singular_name'],
                ];
            }

            if (isset($taxonomy['radio']) && $taxonomy['radio']) {
                $radios[] = $key;
            }

            if (! empty($taxonomy['hideDescription'])) {
                $css .= ".taxonomy-{$key} .term-description-wrap { display: none } ";
            }

            if (! empty($taxonomy['hidePermalink'])) {
                $css .= ".taxonomy-{$key} .term-slug-wrap { display: none } ";
            }

            if (! empty($taxonomy['hideParent'])) {
                $css .= ".taxonomy-{$key} .term-parent-wrap { display: none } ";
            }
        } ?>
        <style>
            <?= $css; ?>
        </style>

        <script>
        document.addEventListener('DOMContentLoaded', function() {
            var radios = <?= json_encode($radios) ?>;
    
            radios.forEach(function(tax) {
                var selector = '#taxonomy-' + tax;
                if (document.querySelector(selector)) {
                    var terms = document.querySelectorAll(selector + ' .selectit input');
    
                    for (var i = 0; i < terms.length; i++) {
                        terms[i].type = 'radio';
                    }
                }
            });
    
            var form = document.querySelector('form#post');
    
            if (form) {
                form.addEventListener('submit', function(event) {
                    var required = <?= json_encode($required) ?>;
                    var messageShown = false;
    
                    required.forEach(function(tax) {
                        if (messageShown) {
                            return false;
                        }
    
                        var selector = '#taxonomy-' + tax.id;
                        if (document.querySelector(selector)) {
                            var activeTerms = document.querySelectorAll(selector + ' .selectit input:checked');
    
                            if (activeTerms.length < 1) {
                                event.preventDefault();
                                messageShown = true;
                                alert('Du måste välja minst en ' + tax.title + ' för posten.');
                                setTimeout(function() {
                                    document.querySelector('#publishing-action .spinner').classList.remove('is-active');
                                    document.querySelector('#publish').classList.remove('disabled');
                                    document.querySelector('#publish').disabled = false;
                                }, 100);
                            }
                        }
                    });
                });
    
                jQuery('#taxonomy-area .selectit input').change(function() {
                    if (this.checked) {
                        const parents = jQuery(this).parents('li');
                        jQuery(parents).each(function() {
                            jQuery(this).find('> .selectit input').prop('checked', true);;
                        });
                    } else {
                        const childs = jQuery(this).closest('li').find('li');
                        jQuery(childs).each(function() {
                            jQuery(this).find('> .selectit input').prop('checked', false);;
                        });
                    }
                });
            }
        }, false);
        </script>
        <?php
    }
}
