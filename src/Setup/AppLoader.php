<?php

namespace WP_Lib\Setup;

use WP_Lib\Admin\PageOrder;
use WP_Lib\Admin\PostsOrder;
use WP_Lib\CustomArchive\CustomArchive;

class AppLoader {
    public function __construct() {
        $this->loadTranslations();
        $this->loadApp();

        new CustomArchive;

        $this->setRobotsTxt();
        $this->createTaxonomies();
        $this->createPostTypes();
        $this->createMenus();
        $this->loadServices();
        $this->setImageSizes();

        if (config('app.sort_pages')) {
            new PageOrder;
        }
    }

    protected function loadApp() {
        $app = new AppContainer(ABSPATH . '../', get_template_directory());

        $classes = config('app.autoload');

        if (empty($classes)) {
            return;
        }

        $created = [];

        foreach ($classes as $class) {
            $created[] = (new $class);
        }

        foreach ($created as $class) {
            if (method_exists($class, 'boot')) {
                $class->boot();
            }
        }
    }

    protected function loadTranslations() {
        load_textdomain('wp_lib', dirname(__FILE__) . '/../resources/lang/sv.mo');
    }

    protected function setRobotsTxt() {
        add_filter('robots_txt', function ($output, $public) {
            if (! $public) {
                return "User-agent: *\nDisallow: /";
            }

            return config('app.robots') ? implode("\n", config('app.robots')) : $output;
        }, 10, 2);
    }

    protected function createPostTypes() : void {
        $css = '';
        $postTypes = config('postTypes');
        $sortablePostTypes = [];

        foreach ($postTypes as $postType => $data) {
            register_post_type($postType, $data);

            if (! empty($data['fontAwesome'])) {
                $css .= ".menu-icon-{$postType} .dashicons-admin-post:before { content: '{$data['fontAwesome']}'; font-family: 'FontAwesome' !important;font-size: 18px !important;}";
            }

            if (! empty($data['sortable'])) {
                $sortablePostTypes[$postType] = $data;
            }

            if (! empty ($data['acfAdminColumns'])) {
                add_filter("manage_{$postType}_posts_columns", function ($columns) use ($data) {
                    $newColumns = [];

                    foreach ($columns as $key => $column) {
                        $newColumns[$key] = $column;

                        if ($key === 'title') {
                            foreach ($data['acfAdminColumns'] as $key => $column) {
                                $newColumns["acf_{$key}"] = $column['label'];
                            }
                        }
                    }

                    return $newColumns;
                });

                add_filter("manage_{$postType}_posts_custom_column", function ($column, $postId) use ($data) {
                    $column = str_replace('acf_', '', $column);

                    if (! empty($data['acfAdminColumns'][$column])) {
                        if (empty($data['acfAdminColumns'][$column]['render'])) {
                            echo get_field($column, $postId);
                        } else {
                            echo $data['acfAdminColumns'][$column]['render']($postId);
                        }
                    }
                }, 10, 2);

                add_filter("manage_edit-{$postType}_sortable_columns", function ($columns) use ($data) {
                    foreach ($data['acfAdminColumns'] as $key => $column) {
                        if (! empty($column['sortable'])) {
                            $columns["acf_{$key}"] = $key;
                        }
                    }

                    return $columns;
                });

                add_action('pre_get_posts', function ($query) use ($postType, $data) {
                    if (! is_admin()) {
                        return;
                    }

                    $quriedPostType = $query->get('post_type') ?? null;

                    if (! $quriedPostType || $quriedPostType !== $postType) {
                        return;
                    }

                    $orderby = $query->get('orderby');

                    foreach ($data['acfAdminColumns'] as $key => $column) {
                        if ($key === $orderby) {
                            $query->set('meta_key', $orderby);
                            $query->set('orderby', $column['orderby'] ?? 'meta_value_num');
                        }
                    }
                });
            }
        }

        if (! empty($sortablePostTypes)) {
            new PostsOrder($sortablePostTypes);
        }

        add_action('admin_head', function () use ($css) {
            ?>
            <style>
                <?= $css ?>
            </style>
            <?php
        });
    }

    protected function createTaxonomies() : void {
        $taxonomies = config('taxonomies');

        foreach ($taxonomies as $taxonomyName => $taxonomy) {
            register_taxonomy($taxonomyName, $taxonomy['post_type'], $taxonomy['settings']);
        }
    }

    protected function createMenus() : void {
        $menus = config('menus');

        foreach ($menus as $location => $description) {
            register_nav_menu($location, $description);
        }
    }

    protected function loadServices() : void {
        $services = config('services');

        if (! empty($services['google_maps_api_key'])) {
            acf_update_setting('google_api_key', $services['google_maps_api_key']);
        }
    }

    protected function removeWpImageSizes() : void {
        $sizes = ['thumbnail', 'medium', 'medium_large', 'large'];

        foreach ($sizes as $size) {
            remove_image_size($size);
        }
    }

    protected function setImageSizes() : void {
        $imageSizes = config('assets.imageSizes');

        if (! $imageSizes) {
            return;
        }

        $this->removeWpImageSizes();

        foreach ($imageSizes as $name => $imageSize) {
            $width = isset($imageSize['width']) ? $imageSize['width'] : 0;
            $height = isset($imageSize['height']) ? $imageSize['height'] : 0;
            $crop = isset($imageSize['crop']) ? $imageSize['crop'] : false;

            add_image_size($name, $width, $height, $crop);
        }
    }
}
