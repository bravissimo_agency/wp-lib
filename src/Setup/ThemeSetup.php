<?php

namespace WP_Lib\Setup;

use Illuminate\Support\Str;

class ThemeSetup {
    public function __construct() {
        $this->removeActionsAndFilters();
        $this->setMaxiumRevisions(2);
        $this->fixNameOnFileUploads();
        $this->removeComments();
        $this->removeCustomizePage();
        $this->fixEditorCap();
        $this->addAdminAssets();
        $this->hideUpdateNoticeForAllButAdmins();
        $this->fixPermalinkForPosts();
        $this->addTemplates();
        $this->hidePostsAdmin();
        $this->wrapEmbeds();
        $this->setAdminFavicon();
        $this->removeDefaultSearchQuery();
        $this->removeAuthorRoute();
        $this->fixRankMath();
        $this->removeSearch();
    }

    public function fixRankMath() {
        add_filter('rank_math/frontend/breadcrumb/items', function ($crumbs, $class) {
            $crumbs = array_map(function ($item) {
                $archivePage = getArchiveId(strtolower($item[0]));

                if ($archivePage) {
                    $item[1] = getArchiveUrl(strtolower($item[0]));
                    $item[0] = get_the_title($archivePage);
                }

                return $item;
            }, $crumbs);

            return $crumbs;
        }, 10, 2);

        add_filter('rank_math/metabox/priority', function ($priority) {
            return 'low';
        });
    }

    public function removeActionsAndFilters() : void {
        add_action('init', function () {
            remove_action('wp_head', 'wp_generator');
            remove_action('wp_head', 'rsd_link');
            remove_action('wp_head', 'wlwmanifest_link');
            remove_action('wp_head', 'wp_resource_hints', 2);
            remove_action('admin_print_styles', 'print_emoji_styles');
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
            remove_filter('the_content_feed', 'wp_staticize_emoji');
            remove_filter('comment_text_rss', 'wp_staticize_emoji');
            remove_action('rest_api_init', 'wp_oembed_register_route');
            remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
            remove_action('wp_head', 'wp_oembed_add_discovery_links');
            remove_action('wp_head', 'wp_oembed_add_host_js');
            remove_action('wp_head', 'rest_output_link_wp_head', 10);
        });

        add_action('wp_enqueue_scripts', function () {
            wp_dequeue_style('wp-block-library');
            wp_dequeue_style('wp-block-library-theme');
            wp_dequeue_style('wc-block-style');
            wp_dequeue_style('global-styles');
        }, 100);
    }

    public function removeDefaultSearchQuery() {
        add_action('posts_request', function ($query) {
            if (is_search() && ! is_admin()) {
                $appContainer = AppContainer::getInstance();

                $defaultSearchQueryMade = $appContainer->attributes['searchQueryMade'] ?? false;

                $appContainer->attributes['searchQueryMade'] = true;

                return $defaultSearchQueryMade ? $query : false;
            }

            return $query;
        });
    }

    public function fixPermalinkForPosts() : void {
        $postArchive = get_post(get_option('page_for_posts'));

        if (empty($postArchive)) {
            return;
        }

        $postArchiveLink = get_the_permalink($postArchive);

        add_filter('post_link', function ($postLink, $postId) use ($postArchiveLink) {
            $post = get_post($postId);

            if (is_object($post) && $post->post_type === 'post') {
                return $postArchiveLink . $post->post_name . '/';
            }
        }, 1, 2);

        add_filter('register_post_type_args', function ($args, $postType) use ($postArchiveLink) {
            if ($postType !== 'post') {
                return $args;
            }

            $postArchiveSlug = makeLinkRelative($postArchiveLink);
            $postArchiveSlug = Str::replaceFirst('/', '', $postArchiveSlug);
            $postArchiveSlug = Str::replaceLast('/', '', $postArchiveSlug);

            $args['rewrite'] = [
                'slug' => $postArchiveSlug,
            ];

            return $args;
        }, 10, 2);
    }

    public function removeComments() : void {
        add_action('admin_menu', function () {
            remove_menu_page('edit-comments.php');
        });

        add_action('wp_before_admin_bar_render', function () {
            global $wp_admin_bar;
            $wp_admin_bar->remove_menu('comments');
        });
    }

    public function removeCustomizePage() : void {
        add_action('admin_menu', function () {
            global $submenu;
            unset($submenu['themes.php'][6]);
        });
    }

    public function hideUpdateNoticeForAllButAdmins() : void {
        if (! current_user_can('update_core')) {
            add_action('admin_head', function () {
                remove_action('admin_notices', 'update_nag', 3);
            }, 1);
        }
    }

    public function setMaxiumRevisions(int $num = 2) : void {
        add_filter('wp_revisions_to_keep', function () use ($num) {
            return $num;
        }, 10, 2);
    }

    public function fixNameOnFileUploads() : void {
        add_filter('wp_handle_upload_prefilter', function ($file) {
            $filenameArr = explode('.', $file['name']);
            $extension = end($filenameArr);
            $fileName = sanitize_title($file['name']);
            $strLength = strlen($extension) + 1;
            $fileName = substr($fileName, 0, -$strLength);
            $file['name'] = $fileName . '.' . $extension;
            $file['name'] = strtolower($file['name']);

            return $file;
        });
    }

    public function fixEditorCap() : void {
        $role_object = get_role('editor');
        $role_object->add_cap('edit_theme_options');

        if (current_user_can('editor')) {
            add_action('admin_menu', function () {
                remove_menu_page('tools.php');
            });
        }
    }

    public function addAdminAssets() : void {
        add_action('admin_enqueue_scripts', function () {
            wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, null);
            wp_enqueue_style('admin-style', get_template_directory_uri() . '/vendor/bravissimo_agency/wp-lib/src/resources/assets/admin.css', false, 'v1');
            wp_enqueue_script('admin-scripts', get_template_directory_uri() . '/vendor/bravissimo_agency/wp-lib/src/resources/assets/admin.js', false, 'v1', true);
        });
    }

    public function addTemplates() : void {
        $templates = config('app.templates');

        if (empty($templates)) {
            return;
        }

        add_filter('theme_page_templates', function () use ($templates) {
            return $templates;
        });
    }

    public function hidePostsAdmin() : void {
        $showPostsAdmin = config('app.show_posts_admin');

        if (! isset($showPostsAdmin) || $showPostsAdmin) {
            return;
        }

        add_action('admin_menu', function () {
            remove_menu_page('edit.php');
        });

        add_action('admin_bar_menu', function ($wpAdminBar) {
            $wpAdminBar->remove_node('new-post');
        }, 999);
    }

    public function wrapEmbeds() {
        add_filter('embed_oembed_html', function ($html) {
            return '<span class="embedWrapper">' . $html . '</span>';
        }, 10, 1);
    }

    public function setAdminFavicon() {
        add_action('admin_head', function () {
            $favicon = get_field('favicon', 'bravissimo');

            if (! empty($favicon)) {
                echo '<link rel="shortcut icon" type="image/png" href="' . $favicon . '">';
            }
        });
    }

    public function removeAuthorRoute() {
        add_action('template_redirect', function () {
            global $wp_query;

            if (is_author()) {
                $wp_query->set_404();
                status_header(404);
            }
        });
    }

    public function removeSearch() {
        if (! config('app.disable_search')) {
            return;
        }

        add_action('parse_query', function ($query) {
            if (! is_admin() && $query->is_search) {
                wp_safe_redirect(config('app.home_url'));
                exit;
            }

            return $query;
        });
    }
}
