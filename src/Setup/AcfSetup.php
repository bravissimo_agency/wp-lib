<?php

namespace WP_Lib\Setup;

use Carbon\Carbon;
use WP_Lib\Models\Acf\File;
use WP_Lib\Models\Acf\FlexibleContentRow;
use WP_Lib\Models\Acf\Image;

class AcfSetup {
    public function __construct() {
        $this->setOptionsPages();
        $this->setDisabledFields();
        $this->setJsonFolder();
        $this->setRelativeLinks();
        $this->relationshipOptions();
        $this->validateFileTypes();
        $this->formatFields();
        $this->setSearchableFields();
    }

    public function formatFields() {
        add_filter('acf/format_value/type=repeater', function ($rows) {
            if (empty($rows)) {
                return $rows;
            }

            return collect($rows)->map(function ($row) {
                return (object) $row;
            });
        }, 100, 1);

        add_filter('acf/format_value/type=flexible_content', function ($rows) {
            if (empty($rows)) {
                return $rows;
            }

            return collect($rows)->map(function ($row) {
                return new FlexibleContentRow($row);
            });
        }, 100, 1);

        add_filter('acf/format_value/type=link', function ($link) {
            if (empty($link) || ! is_array($link)) {
                return $link;
            }

            return (object) $link;
        }, 100, 1);

        add_filter('acf/format_value/type=google_map', function ($googleMap) {
            if (empty($googleMap)) {
                return $googleMap;
            }

            return (object) $googleMap;
        }, 100, 1);

        add_filter('acf/format_value/type=group', function ($group) {
            if (empty($group)) {
                return $group;
            }

            return (object) $group;
        }, 100, 1);

        add_filter('acf/format_value/type=file', function ($file) {
            if (empty($file) || ! is_array($file)) {
                return $file;
            }

            return new File($file);
        }, 100, 1);

        add_filter('acf/format_value/type=date_picker', function ($date) {
            if (empty($date)) {
                return $date;
            }

            return Carbon::parse($date)
                ->locale(get_locale())
                ->settings(['formatFunction' => 'translatedFormat']);
        }, 100, 1);

        add_filter('acf/format_value/type=relationship', function ($posts) {
            $postTypes = config('postTypes');

            $out = [];

            if (empty($posts)) {
                return;
            }

            foreach ($posts as $post) {
                $postType = get_post_type($post);

                if (! empty($postTypes[$postType]) && ! empty($postTypes[$postType]['model'])) {
                    $model = (new $postTypes[$postType]['model'])->find($post);

                    if (method_exists($model, 'fromRelation')) {
                        $model->fromRelation();
                    }

                    $out[] = $model->first();
                }
            }

            if (! empty($out)) {
                return collect($out);
            }

            return $posts;
        }, 100, 1);

        add_filter('acf/format_value/type=post_object', function ($postId) {
            if (empty($postId)) {
                return;
            }

            $postTypes = config('postTypes');

            $postType = get_post_type($postId);

            if (! empty($postTypes[$postType]) && ! empty($postTypes[$postType]['model'])) {
                $model = (new $postTypes[$postType]['model'])->find($postId);

                if (method_exists($model, 'fromRelation')) {
                    $model->fromRelation();
                }

                return $model->first();
            }

            return $postId;
        }, 100, 1);

        $this->formatImageFields();
    }

    public function setOptionsPages() : void {
        $pages = config('acf.optionPages');

        if (! function_exists('acf_add_options_page') || empty($pages)) {
            return;
        }

        foreach ($pages as $page) {
            acf_add_options_page($page);
        }
    }

    public function setDisabledFields() : void {
        $fields = config('acf.disabledFields');

        if (empty($fields)) {
            return;
        }

        foreach ($fields as $field) {
            add_filter("acf/load_field/name={$field}", function ($field) {
                $field['disabled'] = 1;

                return $field;
            });
        }
    }

    public function setJsonFolder() : void {
        $jsonFolder = config('acf.jsonFolder');

        add_filter('acf/settings/save_json', function () use ($jsonFolder) {
            return $jsonFolder;
        });

        add_filter('acf/settings/load_json', function () use ($jsonFolder) {
            return [$jsonFolder];
        });
    }

    public function setRelativeLinks() : void {
        $relativeLinks = config('acf.relativeLinks');

        if (! $relativeLinks) {
            return;
        }

        add_filter('acf/format_value/type=link', function ($value) {
            if (! empty($value)) {
                $value['url'] = makeLinkRelative($value['url']);
            }

            return $value;
        }, 10, 3);
    }

    public function formatImageFields() : void {
        $formatImageData = function ($value) {
            if (! isset($value['sizes'])) {
                return $value;
            }

            $sizes = $value['sizes'];
            unset($sizes['thumbnail']);
            unset($sizes['thumbnail-width']);
            unset($sizes['thumbnail-height']);

            return new Image($value['url'], $value['alt'], $value['caption'], $sizes);
        };

        add_filter('acf/format_value/type=image', function ($image) use ($formatImageData) {
            return $formatImageData($image);
        }, 100, 1);

        add_filter('acf/format_value/type=gallery', function ($items) use ($formatImageData) {
            if (empty($items)) {
                return $items;
            }

            return collect(array_map($formatImageData, $items));
        }, 100, 1);
    }

    public function setSearchableFields() : void {
        $fields = config('acf.searchableFields');

        if (empty($fields)) {
            return;
        }

        add_filter('posts_search', function ($where, $wp_query) use ($fields) {
            global $wpdb;
            global $blog_id;

            if ($blog_id == 1) {
                $blogID = '';
            } else {
                $blogID = $blog_id . '_';
            }

            if (empty($where)) {
                return $where;
            }

            // get search expression
            $terms = $wp_query->query_vars['s'];

            // explode search expression to get search terms
            $exploded = explode(' ', $terms);
            if ($exploded === false || count($exploded) == 0) {
                $exploded = [0 => $terms];
            }

            // reset search in order to rebuilt it as we whish
            $where = '';

            // get searcheable_acf, a list of advanced custom fields you want to search content in
            $list_searcheable_acf = $fields;
            foreach ($exploded as $tag) :
                $where .= '
                  AND (
                    (' . $wpdb->base_prefix . $blogID . "posts.post_title LIKE '%$tag%')
                    OR (" . $wpdb->base_prefix . $blogID . "posts.post_content LIKE '%$tag%')
                    OR EXISTS (
                      SELECT * FROM " . $wpdb->base_prefix . $blogID . 'postmeta
                          WHERE post_id = ' . $wpdb->base_prefix . $blogID . 'posts.ID
                            AND (';
            foreach ($list_searcheable_acf as $searcheable_acf) :
                  if ($searcheable_acf == $list_searcheable_acf[0]):
                    $where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') "; else :
                    $where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
            endif;
            endforeach;
            $where .= ')
                    )
                    OR EXISTS (
                      SELECT * FROM ' . $wpdb->base_prefix . $blogID . 'comments
                      WHERE comment_post_ID = ' . $wpdb->base_prefix . $blogID . "posts.ID
                        AND comment_content LIKE '%$tag%'
                    )
                    OR EXISTS (
                      SELECT * FROM " . $wpdb->base_prefix . $blogID . 'terms
                      INNER JOIN ' . $wpdb->base_prefix . $blogID . 'term_taxonomy
                        ON ' . $wpdb->base_prefix . $blogID . 'term_taxonomy.term_id = ' . $wpdb->base_prefix . $blogID . 'terms.term_id
                      INNER JOIN ' . $wpdb->base_prefix . $blogID . 'term_relationships
                        ON ' . $wpdb->base_prefix . $blogID . 'term_relationships.term_taxonomy_id = ' . $wpdb->base_prefix . $blogID . "term_taxonomy.term_taxonomy_id
                      WHERE (
                        taxonomy = 'post_tag'
                            OR taxonomy = 'category'
                            OR taxonomy = 'myCustomTax'
                        )
                        AND object_id = " . $wpdb->base_prefix . $blogID . 'posts.ID
                        AND ' . $wpdb->base_prefix . $blogID . "terms.name LIKE '%$tag%'
                    )
                )";
            endforeach;

            return $where;
        }, 500, 2);
    }

    public function relationshipOptions() : void {
        add_filter('acf/fields/relationship/query', function ($args, $field, $post_id) {
            $args['post_status'] = ['publish'];
            $args['post__not_in'] = [$post_id];

            return $args;
        }, 10, 3);
    }

    public function validateFileTypes() : void {
        $validateFileTypes = function ($errors, $file, $attachment, $field) {
            if (! in_array($file['type'], ['jpeg', 'jpg', 'png', 'gif'])) {
                $errors[] = __('The uploaded file is not a valid image. Please try again.');
            }

            return $errors;
        };

        add_filter('acf/validate_attachment/type=image', $validateFileTypes, 10, 4);
        add_filter('acf/validate_attachment/type=gallery', $validateFileTypes, 10, 4);
    }
}
