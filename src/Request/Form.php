<?php

namespace WP_Lib\Request;

abstract class Form
{
    abstract public function validate();

    abstract public function submit($request);

    abstract public function successMessage();

    abstract public function rules();

    abstract public function labels();

    public function handle() {
        $this->verifyRecapatcha();

        return $this->validateAndSubmit();
    }

    public function validateAndSubmit() {
        $request = $this->validate();
        $this->submit($request);

        return $this->successResponse();
    }

    public function successResponse() {
        return [
            'status' => 'success',
            'message' => $this->successMessage(),
        ];
    }

    public function errorMessage() {
        return config('form.error_message') ?? __('Something went wrong try again later', 'wp_lib');
    }

    public function verifyRecapatcha() {
        if (empty(config('services.recaptchav3'))) {
            return true;
        }

        $score = verifyRecapatcha(request('gRecaptchaResponse'), 'form');

        if (!$score || $score < config('services.recaptchav3.required_score') ?? 0.4) {
            exit();
        }
    }
}
