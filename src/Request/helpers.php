<?php

use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory as ValidationFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Support\Facades\Http;

/*
// Request helper
*/

function request($key = null, $default = null) {
    if (is_null($key)) {
        return app('request');
    }

    if (is_array($key)) {
        return app('request')->only($key);
    }

    $value = app('request')->__get($key);

    return is_null($value) ? value($default) : $value;
}

/*
// Validation helper
*/

function validate($data, $rules, $labels = []) {
    $loader = new FileLoader(new Filesystem, __DIR__ . '/../resources/lang');
    $translator = new Translator($loader, config('app.locale'));
    $validation = new ValidationFactory($translator, app());

    $errors = null;

    $validator = $validation->make($data, $rules, [], $labels);

    if (! $validator->fails()) {
        return (object) $data;
    }

    $message = config('form.validation_message') ?? __('One or more fields are not filled in correctly, please try again', 'wp_lib');

    $data = [
        'message' => $message,
        'type' => 'validation_error',
        'errors' => $validator->errors(),
    ];

    $res = new JsonResponse($data, 400);

    $res->send();

    exit();
}

function verifyRecapatcha($token, $action = null) {
    $response = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
        'secret'   => config('services.recaptchav3.secret_key'),
        'response' => $token,
        'remoteip' => request()->getClientIp(),
    ]);

    $body = json_decode($response->getBody(), true);

    if (!isset($body['success']) || $body['success'] !== true) {
        return false;
    }

    if ($action && (!isset($body['action']) || $action != $body['action'])) {
        return false;
    }

    return isset($body['score']) ? $body['score'] : false;
}
