<?php

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;

function view($view = null, $data = [], $mergeData = []) {
    echo render($view, $data, $mergeData);
}

function render($view = null, $data = [], $mergeData = []) {
    $factory = app(ViewFactory::class);

    if (func_num_args() === 0) {
        return $factory;
    }

    return $factory->make($view, $data, $mergeData);
}

function asset(?string $path = null) : string {
    return get_template_directory_uri() . '/resources/assets/' . $path;
}

function addJsVariable(string $name, $data) : void {
    app()->addJsVariable($name, $data);
}

function getJsVariables() : array {
    return app()->getJsVariables();
}

function addBladeComponents(array $components) : void {
    foreach ($components as $key => $component) {
        Blade::component($key, $component);
    }
}

function mix(?string $type = null) : ?string {
    $assets = file_get_contents(get_template_directory() . '/mix-manifest.json');
    $assets = json_decode($assets, true);

    if (isHmrMode() && isDev()) {
        $url = file_get_contents(get_template_directory() . '/hot');

        return $url . 'resources/dist/' . $type;
    }

    foreach ($assets as $key => $asset) {
        if (Str::endsWith($key, $type)) {
            return get_template_directory_uri() . $asset;
        }
    }
}

function isHmrMode() {
    return file_exists(get_template_directory() . '/hot');
}

function getSvg(string $name, $class = '') {
    $svg = file_get_contents(get_template_directory() . '/resources/assets/images/' . $name . '.svg');

    if (empty($class)) {
        return $svg;
    }

    $doc = new \DOMDocument;
    $doc->loadXML($svg);
    $svgs = $doc->getElementsByTagName('svg');

    foreach ($svgs as $svg) {
        $svg->setAttribute('class', $class);
    }

    return $doc->saveHTML();
}

/*
//   Get id for frontpage
*/

function frontPageId() : int {
    return (int) get_option('page_on_front');
}

/*
//   Get id for frontpage
*/

function postArchiveId() : int {
    return (int) get_option('page_for_posts');
}

function postArchiveUrl() : string {
    return get_permalink(postArchiveId());
}

/*
// Check if post id is frontpage
*/

function isFrontPage(int $postId) : bool {
    return frontPageId() === $postId;
}

/*
// Check if post id is post archive
*/

function isPostArchive(int $postId) : bool {
    return postArchiveId() === $postId;
}
